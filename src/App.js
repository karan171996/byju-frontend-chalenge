import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
		experience:'',
		location:'',
		skill:'',
		jobs:'',
		showSecondPart:false,
		searchedData:'',
		company:''

    };
	}
	

  locationHandler = (e) =>{
	this.setState({
		location:e.target.value
	})
	}
	

  experienceHandler =(e) =>{
	this.setState({
		experience:e.target.value
	})
  }

  skillHandler =(e) =>{		
	  this.setState({
			skill:e.target.value
		})
	}


	companyHandler =(e) =>{
		this.setState({
			company:e.target.value
		})
		
	}
	checkCompanyName =() =>{
		let newData = this.state.jobs.filter(item=>{
			if(item.companyname.toLowerCase() === this.state.company){
				return item;
			}
		})
	this.setState({
		searchedData:newData
	})

	}




	sortedData =(content) =>{
		console.log(content);

		let newData = this.state.jobs.filter(item => {
			
			if(this.state.experience){
				if(item.experience === this.state.experience){
					return item;
				}
			}
			if(this.state.location){
				if(item.location.indexOf(this.state.location) > -1){
					return item;
				}
			}
			if(this.state.skill){
				if(item.skills.indexOf(this.state.skill) > -1){
					return item;
				}
			}
			if(this.state.skill && this.state.location){
				if((item.skills.indexOf(this.state.skill) > -1) && (item.location.indexOf(this.state.location) > -1)){
					return item
				}
		
			}
			if(this.state.skill && this.state.experience){
				 if((item.skills.indexOf(this.state.skill) > -1) && (item.experience === this.state.experience)){
					return item
				}
			
			}
			if(this.state.location && this.state.experience ){
				if((item.location.indexOf(this.state.location) > -1)&&(item.experience === this.state.experience)){
					return item
				}
			
			}
			if(this.state.skill && this.state.location && this.state.experience){
					if((item.skills.indexOf(this.state.skill) > -1) && (item.location.indexOf(this.state.location) > -1)&&(item.experience === this.state.experience)){
					return item
				}
				
		}
	})
	if(newData.length){
		this.setState({
			showSecondPart:true,
			searchedData:newData
		})
	}

}
SortExp =() =>{
	var newData =this.state.searchedData;
	newData.sort((a,b)=>{
		if(a.experience === b.experience){
			if(a.companyname < b.companyname)
			{return -1;}
			if(a.companyname > b.companyname)
			{return 1;}
			return 0;
		}
		else{
			return 1;
		}
	})
	this.setState({
		searchedData:newData
	})
}

SortLocation =() =>{
	console.log("function called");
	var newData = this.state.searchedData;
		newData.sort((a,b)=>{
		if(a.location !== b.location){
			if(a.location < b.location)
			{return -1;}
			if(a.location > b.location)
			{return 1;}
			return 0;
		}
		else{
			if(a.companyname < b.companyname)
			{return -1;}
			if(a.companyname > b.companyname)
			{return 1;}
			return 0;
		}
	})
	this.setState({

		searchedData:newData
	}
	)
	console.log(this.state.searchedData);
}


  handleSubmit =() =>{
	  const {experience, location, skill} =this.state;
	  if(!experience && !location && !skill){
		  alert("Please select atleast one field");
	  }
	  else{
		fetch(`https://api.myjson.com/bins/kez8a`)
		.then((response)=>response.json())
		.then(data => this.setState({
			jobs:data.jobsfeed,
		
		},()=>{
			this.sortedData(this.state.jobs);
		 }));
		}
		
  }
  render() {
	  console.log(this.state.searchedData ? this.state.searchedData : '');
    return (
      <div className="App">
        <header className="App-header">
         	<div style={{width:'100%'}}>
         	  <label>Search Jobs</label>
         	     	<div className="container1">
         	     	   <div className="sub-conatiner1">
         	     	     <label>experience</label>
         	     	       	<select value={this.state.experience} onChange={(e)=>{this.experienceHandler(e)}}>
										<option value =" "></option>
										<option value="Fresher">Fresher</option>
	         	     	       	<option value="experience" >Experienced</option>
         	     	       	</select>
         	     	   </div>
							<div className="sub-conatiner2" style={{width:'20%'}}>
								<label>location</label>
								<input type="text" value={this.state.location} onChange={(e)=>{this.locationHandler(e)}}/>
							</div>
							<div className="sub-conatiner3" style={{width:'20%'}}>
								<label>skills</label>
								<input type="text" value={this.state.skill} onChange={(e)=>{this.skillHandler(e)}}/>
							</div> 
							<div className="sub-conatiner4" style={{width:'20%'}}>
								<button onClick={this.handleSubmit}>Search</button>
							</div> 
         	     	</div>
         	</div>

				{
					this.state.showSecondPart
					&&
					<div style={{width:'100%',marginTop:'10%'}}>
						<label>Job Listing</label>
						<div>
							<div>
								<label>Company Name</label>
								<input type="text" value={this.state.company} onChange={(e)=>{this.companyHandler(e)}}/>
								<button onClick={()=>{this.checkCompanyName()}}>Search Company</button>
							</div>
							<div>
								<label>total jobs:{this.state.searchedData.length}</label>
							</div>
							<div>
								<button onClick={()=>{this.SortLocation()}}>Sort by Location</button>
								<button onClick={()=>{this.SortExp()}}>Sort by Exp</button>
							</div>

						</div>
						<div className="row" style={{marginLeft:50}}>
							{
								this.state.searchedData.map((item,i) =>{
									return(
									<div key={i} className="column" style={{backgroundColor:'#ccc'}}>
								  		<div>
												<p><b>CompanyName:</b>{item.companyname}</p>
								  		  <p><b>Location:</b>{item.location}</p>
								  		  <p><b>Title:</b>{item.title}</p>
								  		  <p><b>Salary:</b>{item.salary}</p>
								  		  <p><b>Type:</b>{item.type}</p> 
								  		  <p><b>StartDate:</b>{item.startedate}</p>
								  		  <p><b>ApplyLink:</b>{item.applylink}</p>
								  		  <p><b>EndDate:</b>{item.enddate}</p>
								  		  <p><b>Source:</b>{item.source}</p> 

								  		</div>
									</div>
									)
								})
							}
						</div>
					</div>
				}
				
        </header>
      </div>
    );
  }
}

export default App;
